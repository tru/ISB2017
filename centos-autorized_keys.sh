mkdir -p /home/centos/.ssh
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQChyr2SuPSTR18ExCCDHdrn5K+7OHBkHzCNPWYF5rdmaGgsflysqcWcyqeGQu6WPmcJkQQ76n1qjB+4eVzfoB3iINjBcak071M6j+kK8NmIPdgZWm5VtZmODa9DFsByT1tFp1uprgqDP/8LD83hZXuWsAf6P2YDN5xNdQayuy372QuSo5XwY6BdwO+aAOxfPn6UqoEI6yal9wX+CVfjy1jrq1L8pJABMel7wLeK/Qms5d2+SkOu6bh6P3MAwmd5XBbqXhacdonCD/8eM01OS3gEZW/fEBiWMj4dri9+fT1ZQxi9QkaMpeTELqX3uf6aAT5a9VoCgCURZ0ELt2oIEvSD isb2017-centos' > /home/centos/.ssh/authorized_keys

chmod 0700 /home/centos/.ssh
chmod 0600 /home/centos/.ssh/authorized_keys
chown -R centos:centos /home/centos/.ssh
restorecon -rv /home/centos
echo 'centos ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
restorecon -rv /etc/sudoers
