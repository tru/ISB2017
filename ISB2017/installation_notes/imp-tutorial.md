# IMP tutorial (developper version)


# preparing the tarball (for Riccardo)
```
git clone https://github.com/salilab/IMP_tutorial
cd IMP_tutorial/
git fetch
git checkout develop
cd ..
tar czvf /ISB2017/Course_material/July22/IMP_tutorial.tgz  IMP_tutorial/
```

# usage (for the students)
```
cd && tar xzf /ISB2017/Course_material/July22/IMP_tutorial.tgz
module add IMP
cd IMP_tutorial/doc && jupyter notebook
```
