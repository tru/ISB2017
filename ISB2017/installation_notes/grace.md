# grace
- http://plasma-gate.weizmann.ac.il/Grace/

```
wget ftp://plasma-gate.weizmann.ac.il/pub/grace/src/grace5/grace-5.1.25.tar.gz
tar xzvf grace-5.1.25.tar.gz 
cd grace-5.1.25/
./configure --prefix=/ISB2017/grace-5.1.25
sed -i -e 's,/ISB2017/grace-5.1.25,grace,/ISB2017/grace-5.1.25,g' Make.conf
make -j 8
make check && make install
```
