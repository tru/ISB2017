# Download dynamo-v-1.1.286_MCR-9.2.0_GLNXA64_withMCR.tar 

- from https://drive.google.com/uc?id=0B_T9apWxsZjMTEszNGpvVHVuYnM
- requires openmpi-devel

```
mkdir /ISB2017/dynamo-v-1.1.286_MCR-9.2.0_GLNXA64_withMCR
tar -C /ISB2017/dynamo-v-1.1.286_MCR-9.2.0_GLNXA64_withMCR  -xf dynamo-v-1.1.286_MCR-9.2.0_GLNXA64_withMCR.tar
cd /ISB2017/dynamo-v-1.1.286_MCR-9.2.0_GLNXA64_withMCR
export DYNAMO_ROOT=/ISB2017/dynamo-v-1.1.286_MCR-9.2.0_GLNXA64_withMCR
source ${DYNAMO_ROOT}/dynamo_activate_linux.sh
source ${DYNAMO_ROOT}/dynamo_activate_linux_shipped_MCR.sh
module add mpi
./dynamo_compile_mpi.sh mpiCC
```

