Anaconda3: provides python3 with its own conda package management system,
that can be user installed.

+ easy way to take care of python3
+ one shot installation
+ one can use pip to add non anaconda packaged python modules (caveat don't overwrite conda managed packages!)

- as any package management system, you depend on a 3rd party
- some conda packages might have non compatible requirements

Provides  Jupyter Notebook

see list of installed anaconda2 packages at:
(conda list output)
https://gitlab.pasteur.fr/tru/ISB2017/raw/master/ISB2017/installation_notes/anaconda3-list

1) Download and install
```
# Jupyter Notebook and Python/anaconda
cd /ISB2017/sources/
wget https://docs.continuum.io/anaconda/hashes/Anaconda3-4.3.1-Linux-x86_64.sh-hash
wget https://repo.continuum.io/archive/Anaconda3-4.3.1-Linux-x86_64.sh 
# 4447b93d2c779201e5fb50cfc45de0ec96c3804e7ad0fe201ab6b99f73e90302 sha256
# 9209864784250d6855886683ed702846 md5
sh Anaconda3-4.3.1-Linux-x86_64.sh -b -p /ISB2017/anaconda3

```
2) create the modulefile
```
wget -O /ISB2017/modulefiles/anaconda3 https://gitlab.pasteur.fr/tru/ISB2017/raw/master/ISB2017/modulefiles/anaconda3

```
3) update and add more packages
```
module use /ISB2017/modulefiles
module add anaconda3
conda update -y conda
conda update -y --all
conda install biopython

```
