# IMP from https://integrativemodeling.org
- using a separate conda installation tree to avoid conflict from the main anaconda2 tree 
- stable IMP release (not used from ISB2017)
- https://integrativemodeling.org/download-anaconda.html

```
sh /ISB2017/sources/Anaconda2-4.3.1-Linux-x86_64.sh  -b -p /ISB2017/IMP-co-anaconda2
export PATH=/ISB2017/IMP-co-anaconda2/bin:$PATH
conda config --add channels salilab
conda install imp

Fetching package metadata ...........
Solving package specifications: .

Package plan for installation in environment /ISB2017/IMP-co-anaconda2:

The following NEW packages will be INSTALLED:

    boost:    1.61.0-py27_0            
    cgal:     4.9.1-1           salilab
    fftw:     3.3.4-0           salilab
    gmp:      6.1.0-0                  
    gsl:      1.16-0            salilab
    imp:      2.7.0-py27_2      salilab
    libtau:   1.0.1-0           salilab
    mpfr:     3.1.5-0                  
    opencv:   3.1.0-np111py27_1        
    system:   5.8-2                    

The following packages will be UPDATED:

    anaconda: 4.3.1-np111py27_0         --> custom-py27_0
    conda:    4.3.14-py27_0             --> 4.3.22-py27_0

The following packages will be DOWNGRADED due to dependency conflicts:

    jpeg:     9b-0                      --> 8d-2         
    libtiff:  4.0.6-3                   --> 4.0.6-2      
    pillow:   4.0.0-py27_0              --> 3.4.2-py27_0 
    qt:       5.6.2-3                   --> 5.6.2-2      

Proceed ([y]/n)? y

...

[centos@salle3-35 sources]$  conda upgrade --all
Fetching package metadata ...........
Solving package specifications: .

Package plan for installation in environment /ISB2017/IMP-co-anaconda2:

The following NEW packages will be INSTALLED:

    asn1crypto:               0.22.0-py27_0
    bkcharts:                 0.2-py27_0
    bleach:                   1.5.0-py27_0
    html5lib:                 0.999-py27_0
    packaging:                16.8-py27_0
    pandocfilters:            1.4.1-py27_0
    pywavelets:               0.5.2-np112py27_0
    sphinxcontrib:            1.0-py27_0
    sphinxcontrib-websupport: 1.0.1-py27_0
    testpath:                 0.3.1-py27_0
    typing:                   3.6.1-py27_0

The following packages will be UPDATED:

    alabaster:                0.7.9-py27_0       --> 0.7.10-py27_0
    anaconda-client:          1.6.0-py27_0       --> 1.6.3-py27_0
    anaconda-navigator:       1.5.0-py27_0       --> 1.6.3-py27_0
    anaconda-project:         0.4.1-py27_0       --> 0.6.0-py27_0
    astropy:                  1.3-np111py27_0    --> 1.3.3-np112py27_0
    babel:                    2.3.4-py27_0       --> 2.4.0-py27_0
    beautifulsoup4:           4.5.3-py27_0       --> 4.6.0-py27_0
    bokeh:                    0.12.4-py27_0      --> 0.12.6-py27_0
    boto:                     2.45.0-py27_0      --> 2.47.0-py27_0
    bottleneck:               1.2.0-np111py27_0  --> 1.2.1-np112py27_0
    cffi:                     1.9.1-py27_0       --> 1.10.0-py27_0
    chardet:                  2.3.0-py27_0       --> 3.0.4-py27_0
    colorama:                 0.3.7-py27_0       --> 0.3.9-py27_0
    contextlib2:              0.5.4-py27_0       --> 0.5.5-py27_0
    cryptography:             1.7.1-py27_0       --> 1.8.1-py27_0
    dask:                     0.13.0-py27_0      --> 0.15.0-py27_0
    dill:                     0.2.5-py27_0       --> 0.2.6-py27_0
    entrypoints:              0.2.2-py27_0       --> 0.2.2-py27_1
    flask:                    0.12-py27_0        --> 0.12.2-py27_0
    fontconfig:               2.12.1-2           --> 2.12.1-3
    futures:                  3.0.5-py27_0       --> 3.1.1-py27_0
    gevent:                   1.2.1-py27_0       --> 1.2.2-py27_0
    greenlet:                 0.4.11-py27_0      --> 0.4.12-py27_0
    h5py:                     2.6.0-np111py27_2  --> 2.7.0-np112py27_0
    hdf5:                     1.8.17-1           --> 1.8.17-2
    idna:                     2.2-py27_0         --> 2.5-py27_0
    ipykernel:                4.5.2-py27_0       --> 4.6.1-py27_0
    ipython:                  5.1.0-py27_0       --> 5.3.0-py27_0
    ipython_genutils:         0.1.0-py27_0       --> 0.2.0-py27_0
    ipywidgets:               5.2.2-py27_1       --> 6.0.0-py27_0
    isort:                    4.2.5-py27_0       --> 4.2.15-py27_0
    jedi:                     0.9.0-py27_1       --> 0.10.2-py27_2
    jinja2:                   2.9.4-py27_0       --> 2.9.6-py27_0
    jsonschema:               2.5.1-py27_0       --> 2.6.0-py27_0
    jupyter_client:           4.4.0-py27_0       --> 5.1.0-py27_0
    jupyter_console:          5.0.0-py27_0       --> 5.1.0-py27_0
    jupyter_core:             4.2.1-py27_0       --> 4.3.0-py27_0
    lazy-object-proxy:        1.2.2-py27_0       --> 1.3.1-py27_0
    libgcc:                   4.8.5-2            --> 5.2.0-0
    llvmlite:                 0.15.0-py27_0      --> 0.18.0-py27_0
    lxml:                     3.7.2-py27_0       --> 3.8.0-py27_0
    matplotlib:               2.0.0-np111py27_0  --> 2.0.2-np112py27_0
    mistune:                  0.7.3-py27_0       --> 0.7.4-py27_0
    mkl:                      2017.0.1-0         --> 2017.0.3-0
    nbconvert:                4.2.0-py27_0       --> 5.2.1-py27_0
    nbformat:                 4.2.0-py27_0       --> 4.3.0-py27_0
    nltk:                     3.2.2-py27_0       --> 3.2.4-py27_0
    notebook:                 4.3.1-py27_0       --> 5.0.0-py27_0
    numba:                    0.30.1-np111py27_0 --> 0.33.0-np112py27_0
    numexpr:                  2.6.1-np111py27_2  --> 2.6.2-np112py27_0
    numpy:                    1.11.3-py27_0      --> 1.12.1-py27_0
    opencv:                   3.1.0-np111py27_1  --> 3.1.0-np112py27_1
    openpyxl:                 2.4.1-py27_0       --> 2.4.7-py27_0
    openssl:                  1.0.2k-1           --> 1.0.2l-0
    pandas:                   0.19.2-np111py27_1 --> 0.20.2-np112py27_0
    partd:                    0.3.7-py27_0       --> 0.3.8-py27_0
    path.py:                  10.0-py27_0        --> 10.3.1-py27_0
    pathlib2:                 2.2.0-py27_0       --> 2.2.1-py27_0
    ply:                      3.9-py27_0         --> 3.10-py27_0
    prompt_toolkit:           1.0.9-py27_0       --> 1.0.14-py27_0
    psutil:                   5.0.1-py27_0       --> 5.2.2-py27_0
    py:                       1.4.32-py27_0      --> 1.4.34-py27_0
    pyasn1:                   0.1.9-py27_0       --> 0.2.3-py27_0
    pycosat:                  0.6.1-py27_1       --> 0.6.2-py27_0
    pycrypto:                 2.6.1-py27_4       --> 2.6.1-py27_6
    pygments:                 2.1.3-py27_0       --> 2.2.0-py27_0
    pyopenssl:                16.2.0-py27_0      --> 17.0.0-py27_0
    pytables:                 3.3.0-np111py27_0  --> 3.3.0-np112py27_0
    pytest:                   3.0.5-py27_0       --> 3.1.2-py27_0
    pytz:                     2016.10-py27_0     --> 2017.2-py27_0
    qtawesome:                0.4.3-py27_0       --> 0.4.4-py27_0
    qtconsole:                4.2.1-py27_1       --> 4.3.0-py27_0
    requests:                 2.12.4-py27_0      --> 2.14.2-py27_0
    scandir:                  1.4-py27_0         --> 1.5-py27_0
    scikit-image:             0.12.3-np111py27_1 --> 0.13.0-np112py27_0
    scikit-learn:             0.18.1-np111py27_1 --> 0.18.2-np112py27_0
    scipy:                    0.18.1-np111py27_1 --> 0.19.1-np112py27_0
    sphinx:                   1.5.1-py27_0       --> 1.6.2-py27_0
    spyder:                   3.1.2-py27_0       --> 3.1.4-py27_0
    sqlalchemy:               1.1.5-py27_0       --> 1.1.11-py27_0
    statsmodels:              0.6.1-np111py27_1  --> 0.8.0-np112py27_0
    tornado:                  4.4.2-py27_0       --> 4.5.1-py27_0
    traitlets:                4.3.1-py27_0       --> 4.3.2-py27_0
    werkzeug:                 0.11.15-py27_0     --> 0.12.2-py27_0
    widgetsnbextension:       1.2.6-py27_0       --> 2.0.0-py27_0
    wrapt:                    1.10.8-py27_0      --> 1.10.10-py27_0

Proceed ([y]/n)? 

```

# remove the channel (persistent across anaconda versions)
```
conda config --show-sources
conda config --add channels salilab
conda config --remove channels salilab
```
