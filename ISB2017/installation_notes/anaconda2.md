Anaconda2: provides python2 with its own conda package management system,
that can be user installed. If you happen you have non python3 friendly code.

+ easy way to take care of python2
+ one shot installation
+ one can use pip to add non anaconda packaged python modules (caveat don't overwrite conda managed packages!)

- as any package management system, you depend on a 3rd party
- some conda packages might have non compatible requirements

see list of installed anaconda2 packages at:
(conda list output)
https://gitlab.pasteur.fr/tru/ISB2017/raw/master/ISB2017/installation_notes/anaconda2-list

1) Download and install
```
cd /ISB2017/sources/
wget https://repo.continuum.io/archive/Anaconda2-4.3.1-Linux-x86_64.sh
sh /ISB2017/sources/Anaconda2-4.3.1-Linux-x86_64.sh  -b -p /ISB2017/anaconda2
```
2) create the modulefile
```
wget -O /ISB2017/modulefiles/anaconda2 https://gitlab.pasteur.fr/tru/ISB2017/raw/master/ISB2017/modulefiles/anaconda2

```
3) update and add more packages
```
module use /ISB2017/modulefiles
module add anaconda2
conda update -y conda
conda update -y --all
conda install biopython
```

