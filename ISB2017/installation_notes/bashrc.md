# ISB2017 student .bashrc 
- bashrc file additions

```
# ISB2017 modules
module use /ISB2017/modulefiles

# July18 setup
source  /ISB2017/ccp4-7.0/bin/ccp4.setup-sh
source  /ISB2017/phenix-1.11.1-2575/phenix_env.sh

#July19 setup (dynamo and matlab)
# module add matlab dynamo

# July20 setup (aria)
#

# July21 setup (nothing: working on windows)
#

# July 22 setup (IMP)
# module add IMP
```
