# phenix
- https://www.phenix-online.org/

```
To download the PHENIX distribution with a web browser go to:

https://www.phenix-online.org/download/phenix/

When prompted for a user name and password enter:

User Name: download
Password: ********

The password is changed every Monday at 00:05 PST/PDT. (If the password
is expired, simply request a new one at: www.phenix-online.org)

This is an automatically generated message. Please do not reply to
this email. If you experience problems please contact:

download@phenix-online.org

Citing PHENIX:
PHENIX: a comprehensive Python-based system for macromolecular
structure solution. P.D. Adams, P.V. Afonine, G. Bunkoczi, V.B. Chen,
I.W. Davis, N. Echols, J.J. Headd, L.-W. Hung, G.J. Kapral,
R.W. Grosse-Kunstleve, A.J. McCoy, N.W. Moriarty, R. Oeffner,
R.J. Read, D.C. Richardson, J.S. Richardson, T.C. Terwilliger and
P.H. Zwart. Acta Cryst. D66, 213-221 (2010).
```

- install into /ISB2017/phenix-1.11.1-2575

```
tar xjf /ISB2017/sources/phenix-installer-1.11.1-2575-intel-linux-2.6-x86_64-centos6.tar.gz
cd phenix-installer-1.11.1-2575-intel-linux-2.6-x86_64-centos6/
alias pymol=/ISB2017/anaconda2/bin/pymol
./install --prefix=/ISB2017/
```

Usage:
```
source /ISB2017/phenix-1.11.1-2575/phenix_env.sh
phenix
```
