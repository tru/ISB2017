# IMP (development version)
- reuse anaconda2 setup

```
module purge
module add anaconda2
git clone https://github.com/salilab/imp
mkdir imp-build
cd imp-build
cmake ../imp  -DCMAKE_BUILD_TYPE=Release -DIMP_MAX_LOG=SILENT \
-DIMP_MAX_CHECKS=NONE \
-DCMAKE_INCLUDE_PATH=/ISB2017/anaconda2/include/ \
-DCMAKE_LIBRARY_PATH=/ISB2017/anaconda2/lib \
-DHDF5_INCLUDE_DIR=/ISB2017/anaconda2/include \
-DCMAKE_INSTALL_PREFIX=/ISB2017/IMP
make -j 4 && make install
cp ~/imp/modules/pmi/pyext/src/process_output.py /ISB2017/IMP/bin/
```

add the IMP modulefile
```
#%Module1.0#####################################################################
##
## IMP-co-anaconda2 modulefile
##
proc ModulesHelp { } {
        global dotversion

        puts stderr "\tAdds `/ISB2017/IMP/bin' to your PATH environment variable"
}

module-whatis   "adds `/ISB2017/IMP/bin' to your PATH environment variable"

prepend-path    PATH    /ISB2017/IMP/bin
prepend-path    PYTHONPATH      /ISB2017/IMP/lib64/python2.7/site-packages/
prepend-path    LD_LIBRARY_PATH /ISB2017/IMP/lib64
module add anaconda2
# added for Riccardo practical
module add chimera-1.11.2-linux_x86_64
module add grace-5.1.25
```

