# CCP4 -7.0 64 bits for linux
- http://www.ccp4.ac.uk/download/index.php#os=linux
- CCP4 Program Suite v7.0 including SHELX (64 bit) -> ccp4-7.0-shelx-linux-x86_64.tar.bz2
```
cd /ISB2017 && tar xjf ccp4-7.0-shelx-linux-x86_64.tar.bz2
cd /ISB2017/ccp4-7.0/ && ./BINARY.setup
```

Before using for bash users:
```
source /ISB2017/ccp4-7.0/bin/ccp4.setup-sh
```
