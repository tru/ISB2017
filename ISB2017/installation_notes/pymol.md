# pymol 
```
cd /ISB2017/sources
wget -O pymol-v1.8.6.0.tar.bz2 https://sourceforge.net/projects/pymol/files/pymol/1.8/pymol-v1.8.6.0.tar.bz2/download
sudo yum -y install glew-devel glew msgpack-devel
tar -C ~/build -xjvf /ISB2017/sources/pymol-v1.8.6.0.tar.bz2
cd ~/build/pymol
python setup.py build
python setup.py install --bundled-pmw
```

Alternatively, you can pull the subsersion code with:
```
svn checkout https://svn.code.sf.net/p/pymol/code/trunk pymol-code
```

python3(anaconda3) will need Pmw2(https://downloads.sourceforge.net/project/pmw/Pmw-2.0.0.tar.gz?) instead of the bundled pwm.
