mkdir -p /home/isb2017/.ssh
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDJS7wW19JQ6l7p4W4CwfNvWT8MmwFwneH5/c+uI5w48DzypdrBHBYwEBu10UrlYbgTjQX5rU67Wu/yYXnLPOjowex/j2ie3TY5wTuEnuJuU8YphPbZaGP4RyU0rAeS51iH0FoTmEcsSbpQPLPt/nGNuPdtZhMsnpdboKY7aZcWLGarIaS50WnrQgQMZ49m7y06Sf+rze8dJBO8w6rVDIoWqveuRqjggI2HjMmH3atJH9QKSrQPlOeV4XAkAiSj1JyQBTCOIGjg18jK7bIQnbGmF/hZdm6jZxmiz+jaGj77e8oJtMhi9bL0uEyOfnqcINsU18rqlqIOLJUJN6Gj1AQh isb2017-student' >/home/isb2017/.ssh/authorized_keys 
chmod 0700 /home/isb2017/.ssh
chmod 0600 /home/isb2017/.ssh/authorized_keys
chown -R isb2017:isb2017 /home/isb2017/.ssh
restorecon -rv /home/isb2017
